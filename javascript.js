function scrollToTop() {
    scrollToSlide("menu");
}

function scrollToSlide(id) {
    var element = document.getElementById(id);
    element.scrollIntoView();
}

function makeManAppear() {
    document.getElementById("sprite").style.visibility = "visible";
    setTimeout(function(){
        moveManStart();
    }, 100);
}

function moveManMiddle() {
    document.getElementById("sprite").style.top = "50%";
}

function moveManStart() {
    document.getElementById("sprite").style.top = "calc(80% - 100px)";
}

function moveManEnd() {
    document.getElementById("sprite").style.top = "calc(100% - 100px)";
}

window.addEventListener("load", scrollToTop);
document.addEventListener("load", scrollToTop);